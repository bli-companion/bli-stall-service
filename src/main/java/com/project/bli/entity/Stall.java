package com.project.bli.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

@Entity
@Table(name="stall")
public class Stall {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="STALL_SEQ")
	@SequenceGenerator(sequenceName="stall_seq", allocationSize=1, name="STALL_SEQ")
	private int id;
	
	@NotNull
	private String location;
	
	@Nullable
	private String name;
	
	@Nullable
	private String description;
	
	@Nullable
	private int stock;
	
	@Nullable
	private int queue_stock;
	
	@Nullable
	private int reset_stock;
	
	@Nullable
	private int soldout_counter;
	
	@Column(nullable=false, updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date created_at;
	
	@NotNull
	private String created_by;
	
	@Column(nullable=true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated_at;
	
	@Nullable
	private String updated_by;
	
	private boolean is_deleted = false;
	
	public Stall() {
		
	}
	
	public Stall(String location, String name, String description, 
			int stock, int queue_stock, int reset_stock, int soldout_counter,
			Date created_at, String created_by, Date updated_at, String updated_by,
			boolean is_deleted) {
		this.location = location;
		this.name = name;
		this.description = description;
		this.stock = stock;
		this.queue_stock = queue_stock;
		this.reset_stock = reset_stock;
		this.soldout_counter = soldout_counter;
		this.created_at = created_at;
		this.created_by = created_by;
		this.updated_at = updated_at;
		this.updated_by = updated_by;
		this.is_deleted = is_deleted;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getQueue_stock() {
		return queue_stock;
	}

	public void setQueue_stock(int queue_stock) {
		this.queue_stock = queue_stock;
	}

	public int getReset_stock() {
		return reset_stock;
	}

	public void setReset_stock(int reset_stock) {
		this.reset_stock = reset_stock;
	}
	
	public int getSoldout_counter() {
		return soldout_counter;
	}

	public void setSoldout_counter(int soldout_counter) {
		this.soldout_counter = soldout_counter;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	public String getUpdated_by() {
		return updated_by;
	}

	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}

	public boolean isIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	@Override
	public String toString() {
		return "Stall [id=" + id + ", location=" + location + ", name=" + name + ", description=" + description
				+ ", stock=" + stock + ", queue_stock=" + queue_stock + ", reset_stock=" + reset_stock
				+ ", soldout_counter=" + soldout_counter + ", created_at=" + created_at + ", created_by=" + created_by
				+ ", updated_at=" + updated_at + ", updated_by=" + updated_by + ", is_deleted=" + is_deleted + "]";
	}
	
}

package com.project.bli.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "vendor")
public class Vendor {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VENDOR_SEQ")
	@SequenceGenerator(sequenceName="vendor_seq", allocationSize=1, name="VENDOR_SEQ")
	private Integer id;
	
	@NotNull
	@Column(name="vendor_staff_nip")
	private String vendor_staff_nip;
	
	@NotNull
	@Column(name="vendor_stall_id")
	private Integer vendor_stall_id;
	
	public Vendor() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getVendor_staff_nip() {
		return vendor_staff_nip;
	}

	public void setVendor_staff_nip(String vendor_staff_nip) {
		this.vendor_staff_nip = vendor_staff_nip;
	}

	public Integer getVendor_stall_id() {
		return vendor_stall_id;
	}

	public void setVendor_stall_id(Integer vendor_stall_id) {
		this.vendor_stall_id = vendor_stall_id;
	}

	@Override
	public String toString() {
		return "Vendor [id=" + id + ", vendor_staff_nip=" + vendor_staff_nip + ", vendor_stall_id=" + vendor_stall_id
				+ "]";
	}
	
}

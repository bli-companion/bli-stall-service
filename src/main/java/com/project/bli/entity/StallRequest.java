package com.project.bli.entity;

import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

public class StallRequest {
	
	@NotNull
	private String location;
	
	@NotNull
	private String name;
	
	@Nullable
	private String description;
	
	@NotNull
	private int stock;
	
	@NotNull
	private int queue_stock;
	
	@NotNull
	private int reset_stock;
	
	@Nullable
	private int soldout_counter;
	
	@NotNull
	private String staff_nip;
	
	@NotNull
	private String created_by;
	
	public StallRequest() {
		
	}
	
	public StallRequest(String location, String name, String description, 
			int stock, int queue_stock, int reset_stock, int soldout_counter,
			String staff_nip, String created_by) {
		this.location = location;
		this.name = name;
		this.description = description;
		this.stock = stock;
		this.queue_stock = queue_stock;
		this.reset_stock = reset_stock;
		this.soldout_counter = soldout_counter;
		this.staff_nip = staff_nip;
		this.created_by = created_by;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getQueue_stock() {
		return queue_stock;
	}

	public void setQueue_stock(int queue_stock) {
		this.queue_stock = queue_stock;
	}

	public int getReset_stock() {
		return reset_stock;
	}

	public void setReset_stock(int reset_stock) {
		this.reset_stock = reset_stock;
	}

	public int getSoldout_counter() {
		return soldout_counter;
	}

	public void setSoldout_counter(int soldout_counter) {
		this.soldout_counter = soldout_counter;
	}

	public String getStaff_nip() {
		return staff_nip;
	}

	public void setStaff_nip(String staff_nip) {
		this.staff_nip = staff_nip;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	@Override
	public String toString() {
		return "StallRequest [location=" + location + ", name=" + name + ", description=" + description + ", stock="
				+ stock + ", queue_stock=" + queue_stock + ", reset_stock=" + reset_stock + ", soldout_counter="
				+ soldout_counter + ", staff_nip=" + staff_nip + ", created_by=" + created_by + "]";
	}
	
}

package com.project.bli.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.lang.Nullable;

@Entity
@Table(name = "staff")
public class Staff {

	@Id
	private String nip;
	
	@NotNull
	@Size(max = 100)
	private String name;
	
	@NotNull
	@Temporal(TemporalType.DATE)
	private Date dob;
	
	@Nullable
	private String domain;
	
	@Nullable
	@Size(max = 100)
	private String program;
	
	@NotNull
	private boolean internal;
	
	@NotNull
	private Role role;
	
	@NotNull
	private boolean flag_trainer;
	
	@NotNull
	private boolean flag_trainee;
	
	@NotNull
	@Column(nullable=false, updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date created_at;
	
	@NotNull
	private String created_by;
	
	@Column(nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated_at;
	
	private String updated_by;
	
	@NotNull
	private boolean is_deleted;

	public Staff() {
		
	}
	
	public Staff(String name, Date dob, String domain, 
			String program, boolean internal, Role role, boolean flag_trainer,
			boolean flag_trainee, Date created_at, String created_by,
			Date updated_at, String updated_by, boolean is_deleted) {
		this.name = name;
		this.dob = dob;
		this.domain = domain;
		this.program = program;
		this.internal = internal;
		this.role = role;
		this.flag_trainer = flag_trainer;
		this.flag_trainee = flag_trainee;
		this.created_at = created_at;
		this.created_by = created_by;
		this.updated_at = updated_at;
		this.updated_by = updated_by;
		this.is_deleted = is_deleted;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getProgram() {
		return program;
	}

	public void setProgram(String program) {
		this.program = program;
	}

	public boolean isInternal() {
		return internal;
	}

	public void setInternal(boolean internal) {
		this.internal = internal;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public boolean isFlag_trainer() {
		return flag_trainer;
	}

	public void setFlag_trainer(boolean flag_trainer) {
		this.flag_trainer = flag_trainer;
	}

	public boolean isFlag_trainee() {
		return flag_trainee;
	}

	public void setFlag_trainee(boolean flag_trainee) {
		this.flag_trainee = flag_trainee;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	public String getUpdated_by() {
		return updated_by;
	}

	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}

	public boolean isIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	@Override
	public String toString() {
		return "Staff [nip=" + nip + ", name=" + name + ", dob=" + dob + ", domain=" + domain + ", program=" + program
				+ ", internal=" + internal + ", role=" + role + ", flag_trainer=" + flag_trainer + ", flag_trainee="
				+ flag_trainee + ", created_at=" + created_at + ", created_by=" + created_by + ", updated_at="
				+ updated_at + ", updated_by=" + updated_by + ", is_deleted=" + is_deleted + "]";
	}
	
}

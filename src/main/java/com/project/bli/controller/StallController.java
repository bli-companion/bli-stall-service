package com.project.bli.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.project.bli.entity.Stall;
import com.project.bli.entity.StallRequest;
import com.project.bli.service.impl.StallServiceImpl;

@RestController
@RequestMapping("api/stall")
public class StallController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StallController.class);
	
	@Autowired
	private StallServiceImpl stallServiceImpl;
	
	@GetMapping
	public ResponseEntity<List<Stall>> getAllStalls() {
		LOGGER.info("Getting all stalls ...");
		List<Stall> stallList = stallServiceImpl.getStalls();
		LOGGER.info("Returning all stalls ...");
		return new ResponseEntity<List<Stall>>(stallList, HttpStatus.OK);
	}
	
	@GetMapping("/location/{location}")
	public ResponseEntity<List<Stall>> getStallByLocation(@PathVariable String location) {
		LOGGER.info("Getting stall(s) by location ...");
		List<Stall> stallList = stallServiceImpl.getStallByLocation(location);
		LOGGER.info("Returning stall(s) ...");
		return new ResponseEntity<List<Stall>>(stallList, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Void> addStall(@RequestBody StallRequest stallRequest, UriComponentsBuilder builder) {
		LOGGER.info("Add stall controller triggered ...");
		boolean flag = stallServiceImpl.addStall(stallRequest);
		
		if (!flag) {
			LOGGER.info("Adding stall issue / stall already registered ...");
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/stall/{id}").buildAndExpand(stallRequest.getName()).toUri());
		LOGGER.info("Stall successfully added ...");
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
	
	@PutMapping
	public ResponseEntity<Stall> updateStall(@RequestBody Stall stall) {
		LOGGER.info("Update stall controller triggered ...");
		stallServiceImpl.updateStall(stall);
		LOGGER.info("Stall successfully updated ...");
		return new ResponseEntity<Stall>(stall, HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Void> deleteStall(@PathVariable Integer id) {
		LOGGER.info("Delete stall controller triggered ...");
		stallServiceImpl.deleteStall(id);
		LOGGER.info("Stall successfully deleted ...");
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/id")
	public ResponseEntity<List<Integer>> getAllStallId() {
		LOGGER.info("Retrieving all stall ID ...");
		List<Integer> list = stallServiceImpl.getAllStallsId();
		LOGGER.info("Returning all stall ID ...");
		return new ResponseEntity<List<Integer>>(list, HttpStatus.OK);
	}
	
	@GetMapping("/detail/{id}")
	public ResponseEntity<Stall> getOneStallById(@PathVariable Integer id) {
		LOGGER.info("Getting stall detail by ID ...");
		Stall stall = stallServiceImpl.getOneStallById(id);
		LOGGER.info("Returning stall detail ...");
		return new ResponseEntity<Stall>(stall, HttpStatus.OK);
	}
	
	@GetMapping("/stock/check/{location}")
	public ResponseEntity<List<Integer>> checkStock(@PathVariable String location) {
		LOGGER.info("Retrieving list of stock by location ...");
		List<Integer> list = stallServiceImpl.checkStock(location);
		LOGGER.info("Returning list of stock ...");
		return new ResponseEntity<List<Integer>>(list, HttpStatus.OK);
	}
	
	@GetMapping("/stock/reduce/{id}")
	public ResponseEntity<Void> reduceStock(@PathVariable Integer id) {
		LOGGER.info("Reduce stock controller triggered ...");
		boolean flag = stallServiceImpl.reduceStock(id);
		if (!flag) {
			LOGGER.info("Reducing stock unsuccessful ...");
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
		LOGGER.info("Reducing stock by ID successful ...");
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/stock/reset")
	public ResponseEntity<Void> resetStock() {
		LOGGER.info("Reset stock controller triggered ...");
		stallServiceImpl.resetStock();
		LOGGER.info("Successfully resetting all stall stock ...");
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/vendor/id/{nip}")
	public ResponseEntity<List<Integer>> getStallIdByVendorId(@PathVariable Integer nip) {
		LOGGER.info("Get stall ID by vendor NIP controller triggered!");
		List<Integer> stallIdList = stallServiceImpl.getStallIdByVendorId(nip);
		return new ResponseEntity<List<Integer>>(stallIdList, HttpStatus.OK);
	}
	
	@GetMapping("/vendor/soldout/{id}")
	public ResponseEntity<List<Integer>> getSoldOut(@PathVariable Integer id) {
		LOGGER.info("Get stall soldout count controller triggered!");
		List<Integer> soldoutCounter = stallServiceImpl.getSoldOut(id);
		return new ResponseEntity<List<Integer>>(soldoutCounter, HttpStatus.OK);
	}

}

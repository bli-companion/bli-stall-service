package com.project.bli.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.project.bli.entity.Staff;
import com.project.bli.entity.Vendor;

@Repository
public interface VendorRepository extends JpaRepository<Vendor, Integer> {
	@Query(
		value="SELECT * FROM vendor WHERE vendor_staff_nip=?1",
		nativeQuery=true)
	Vendor findByStaff(String nip);
}

package com.project.bli.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.project.bli.dao.VendorDAO;
import com.project.bli.entity.Vendor;

@Repository
@Transactional
public class VendorDAOImpl implements VendorDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(VendorDAOImpl.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public boolean addVendor(Vendor vendor) {
		LOGGER.info("Add vendor DAO triggered!");
		String sql = "INSERT INTO vendor (id, vendor_staff_nip, vendor_stall_id)"
				+ "VALUES(vendor_seq.nextval, ?, ?)";
		int rowAffected = jdbcTemplate.update(sql, vendor.getVendor_staff_nip(), vendor.getVendor_stall_id());
		LOGGER.info("Checking affected row ...");
		if (rowAffected > 0) {
			LOGGER.info("New vendor successfully inserted ...");
			return true;
		}
		LOGGER.info("Failed to insert new vendor ...");
		return false;
	}

	@Override
	public boolean isExists(String nip) {
		LOGGER.info("Check if exist DAO triggered!");
//		Vendor vendor = vendorRepository.findByStaff(nip);
		String sql = "SELECT COUNT(1) FROM vendor WHERE vendor_staff_nip = ?";
		int rowAffected = jdbcTemplate.queryForObject(sql, Integer.class, nip);
		System.out.println("Staff exists row affected: " + rowAffected);
//		String getNip = rowAffected.getVendor_staff_nip();
		if (rowAffected > 0) {
			LOGGER.info("It appears staff exists ...");
			return true;
		} else {
			LOGGER.info("It appears staff doesn't exists ...");
			return false;
		}
	}

}

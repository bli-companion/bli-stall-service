package com.project.bli.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.project.bli.dao.StallDAO;
import com.project.bli.entity.Role;
import com.project.bli.entity.Staff;
import com.project.bli.entity.Stall;
import com.project.bli.entity.StallRequest;
import com.project.bli.entity.Vendor;
import com.project.bli.service.impl.StaffServiceImpl;

@Repository
@Transactional
public class StallDAOImpl implements StallDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StallDAOImpl.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Autowired
	private VendorDAOImpl vendorDAOImpl;
	
	@Autowired
	private StaffServiceImpl staffServiceImpl;
	
	@Autowired
	private RestTemplate restTemplate;

	@Override
	public List<Stall> getStalls() {
		LOGGER.info("Get stall(s) DAO triggered!");
		String sql = "SELECT * FROM stall";
		RowMapper<Stall> rowMapper = new BeanPropertyRowMapper<Stall>(Stall.class);
		return this.jdbcTemplate.query(sql, rowMapper);
	}

	@Override
	public List<Stall> getStallByLocation(String location) {
		LOGGER.info("Get stall(s) by location DAO triggered!");
		String sql = "SELECT id, location, name, description, stock, queue_stock "
				+ "FROM stall WHERE location = ?";
		List<Stall> listData = jdbcTemplate.query(
				sql, 
				new Object[] { location }, 
				new BeanPropertyRowMapper<Stall>(Stall.class));
		return listData;
	}

	@Override
	public boolean isExists(Integer id) {
		LOGGER.info("Check if stall is exists DAO triggered!");
		String sql = "SELECT COUNT(1) FROM stall WHERE id = ?";
		Integer count = jdbcTemplate.queryForObject(sql, Integer.class, id);
		return count != 0 && count > 0;
	}
	
	@Override
	public boolean addStall(StallRequest stallRequest) {
		LOGGER.info("Add stall service triggered!");
		
		// Query user to get ROLE to check if current user has VENDOR role
        Staff staff = getStaffDetail(stallRequest.getStaff_nip()).getBody();
        
        // Check vendor table if current user already exists
        boolean vendorExistsFlag = vendorDAOImpl.isExists(staff.getNip());
        
        if (vendorExistsFlag) {
        	LOGGER.info("Current staff already has Stall ...");
        	return false;
        }
        
		final KeyHolder holder = new GeneratedKeyHolder();
		
		String sql = "INSERT INTO stall (id, location, name, description, stock, queue_stock, "
				+ "reset_stock, soldout_counter, created_at, created_by, is_deleted) "
				+ "VALUES (stall_seq.nextval, :locationr, :namer, :descriptionr, :stockr, :queue_stockr, "
				+ ":reset_stockr, :soldout_counterr, :created_atr, :created_byr, :is_deletedr)";
		
		MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("locationr", stallRequest.getLocation());
        parameters.addValue("namer", stallRequest.getName());
        parameters.addValue("descriptionr", stallRequest.getDescription());
        parameters.addValue("stockr", stallRequest.getStock());
        parameters.addValue("queue_stockr", stallRequest.getQueue_stock());
        parameters.addValue("reset_stockr", stallRequest.getReset_stock());
        parameters.addValue("soldout_counterr", stallRequest.getSoldout_counter());
        parameters.addValue("created_atr", new Date());
        parameters.addValue("created_byr", stallRequest.getCreated_by());
        parameters.addValue("is_deletedr", false);
        
        int rowAffected = namedParameterJdbcTemplate.update(sql, parameters, holder, new String[]{"id"});
        int stallId = holder.getKey().intValue();
        
        if (rowAffected == 0) {
        	LOGGER.info("Failed to add new stall ...");
        	return false;
        }
        
        LOGGER.info("Checking user role ...");
        if (staff.getRole().equals(Role.ROLE_SUPERADMIN) || staff.getRole().equals(Role.ROLE_ADMIN)) {
        	Vendor vendor = new Vendor();
            vendor.setVendor_staff_nip(staff.getNip());
            vendor.setVendor_stall_id(stallId);
            
        	boolean flag = vendorDAOImpl.addVendor(vendor);
            if (flag) {
            	LOGGER.info("Adding staff NIP and stall ID to vendor potentially successful ...");
            	return true;
            }
            LOGGER.info("Failed to insert staff NIP and stall ID to Vendor table ...");
            return false;
        }
        
        LOGGER.info("Failed to insert staff NIP and stall ID to Vendor table ...");
        return false;
	}

	@Override
	public void updateStall(Stall stall) {
		LOGGER.info("Update stall DAO triggered!");
		String sql = "UPDATE stall SET location=?, name=?, description=?, stock=?, queue_stock=?,"
				+ "reset_stock=?, soldout_counter=?, updated_at=?, updated_by=? WHERE id=?";
		jdbcTemplate.update(sql,
				stall.getLocation(), stall.getName(), stall.getDescription(), stall.getStock(),
				stall.getQueue_stock(), stall.getReset_stock(), stall.getSoldout_counter(), stall.getUpdated_at(),
				stall.getUpdated_by(), stall.getId());		
	}

	@Override
	public void deleteStall(Integer id) {
		LOGGER.info("Delete stall DAO triggered!");
		String sql = "DELETE FROM stall WHERE id=?";
		jdbcTemplate.update(sql, id);
	}

	@Override
	public List<Integer> getAllStallsId() {
		LOGGER.info("Get list of stall ID DAO triggered!");
		String sql = "SELECT id FROM stall";
		return jdbcTemplate.query(sql, new RowMapper<Integer>() {
			public Integer mapRow(ResultSet rs, int row) throws SQLException {
				return rs.getInt(1);
			}
		});
	}

	@Override
	public Stall getOneStallById(Integer id) {
		LOGGER.info("Get one stall by ID DAO triggered!");
		String sql = "SELECT * FROM stall WHERE id=?";
		RowMapper<Stall> rowMapper = new BeanPropertyRowMapper<Stall>(Stall.class);
		Stall stall = jdbcTemplate.queryForObject(sql, rowMapper, id);
		return stall;
	}

	@Override
	public List<Integer> getStallIdByVendorId(Integer id) {
		LOGGER.info("Get stall ID by vendor NIP DAO triggered!");
		return jdbcTemplate.query("SELECT vendor_stall_id FROM vendor WHERE vendor_staff_nip=" + id, 
			new RowMapper<Integer>() {
			public Integer mapRow(ResultSet rs, int row) throws SQLException {
				return rs.getInt(1);
			}
		});
	}

	@Override
	public List<Integer> checkStock(String location) {
		LOGGER.info("Check stock DAO triggered!");
		String sql = "SELECT stock FROM stall WHERE location=?";
		return jdbcTemplate.query(sql, new RowMapper<Integer>() {
			public Integer mapRow(ResultSet rs, int row) throws SQLException {
				return rs.getInt(1);
			}
		}, location);
	}

	@Override
	public void reduceStock(Integer id) {
		LOGGER.info("Reduce stock by stall ID DAO triggered!");
		String sql = "UPDATE stall SET stock = (stock - 1) WHERE id = ?";
		int count = jdbcTemplate.update(sql, id);
		System.out.println("Reduced ID: " + id + ", updated rows: " + count);
	}

	@Override
	public void resetStock() {
		LOGGER.info("Reset stock DAO triggered!");
		String sql = "UPDATE stall SET stock = reset_stock";
		jdbcTemplate.update(sql);	
	}

	@Override
	public List<Integer> getSoldOut(Integer id) {
		LOGGER.info("Get soldout counter DAO triggered!");
		String sql = "SELECT soldout_counter FROM stall a, vendor b WHERE a.id = b.vendor_stall_id"
				+ " AND b.vendor_stall_id =" + id;
		return jdbcTemplate.query(sql, new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getInt(1);
			}
		});
	}

	@Override
	public void addSoldOut(Integer id) {
		String sql = "UPDATE stall SET soldout_counter = (soldout_counter + 1) WHERE id = ?";
		int count = jdbcTemplate.update(sql, id);
		System.out.println("Soldout added to ID: " + id + ", updated rows: " + count);
	}
	
	public ResponseEntity<Staff> getStaffDetail(String nip) {
        LOGGER.info("Attempting to get staff detail ...");
        String url = "http://bli-staff-service/api/staff/detail/" + nip;
        ResponseEntity<Staff> responseStaff = 
        		restTemplate.exchange(url, HttpMethod.GET, null, Staff.class);
        return responseStaff;
	}

}

package com.project.bli.dao;

import com.project.bli.entity.Vendor;

public interface VendorDAO {

	boolean addVendor(Vendor vendor);
	boolean isExists(String nip);
	
}

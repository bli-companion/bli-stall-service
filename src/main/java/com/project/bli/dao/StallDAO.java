package com.project.bli.dao;

import java.util.List;

import com.project.bli.entity.Stall;
import com.project.bli.entity.StallRequest;

public interface StallDAO {
	
	List<Stall> getStalls();
	List<Stall> getStallByLocation(String location);
	boolean isExists(Integer id);
	boolean addStall(StallRequest stallRequest);	// Changes
	void updateStall(Stall stall);
	void deleteStall(Integer id);
	List<Integer> getAllStallsId();
	Stall getOneStallById(Integer id);
	List<Integer> getStallIdByVendorId(Integer id);
	
	List<Integer> checkStock(String location);
	void reduceStock(Integer id);
	void resetStock();
	
	List<Integer> getSoldOut(Integer id);
	void addSoldOut(Integer id);

}

package com.project.bli.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.project.bli.dao.impl.StallDAOImpl;
import com.project.bli.entity.Stall;
import com.project.bli.entity.StallRequest;
import com.project.bli.service.StallService;

@Service
public class StallServiceImpl implements StallService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StallServiceImpl.class);
	
	@Autowired
	private StallDAOImpl stallDAOImpl;

	@Override
	public List<Stall> getStalls() {
		return stallDAOImpl.getStalls();
	}

	@Override
	public List<Stall> getStallByLocation(String location) {
		return stallDAOImpl.getStallByLocation(location);
	}

	@Override
	@HystrixCommand(fallbackMethod = "emptyStaff",
		commandProperties = {
		       @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
		       @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value="60")
	    })
	public boolean addStall(StallRequest stallRequest) {
		boolean flag = stallDAOImpl.addStall(stallRequest);
		if (flag) {
			return true;
		}
		return false;
	}

	@Override
	public void updateStall(Stall stall) {
		stallDAOImpl.updateStall(stall);		
	}

	@Override
	public void deleteStall(Integer id) {
		stallDAOImpl.deleteStall(id);		
	}

	@Override
	public List<Integer> getAllStallsId() {
		return stallDAOImpl.getAllStallsId();
	}

	@Override
	public Stall getOneStallById(Integer id) {
		return stallDAOImpl.getOneStallById(id);
	}

	@Override
	public List<Integer> getStallIdByVendorId(Integer id) {
		List<Integer> stallIdList = stallDAOImpl.getStallIdByVendorId(id);
		return stallIdList;
	}

	@Override
	public List<Integer> checkStock(String location) {
		return stallDAOImpl.checkStock(location);
	}

	@Override
	public boolean reduceStock(Integer id) {
		Stall stall = stallDAOImpl.getOneStallById(id);
		
		if (stall.getStock() == 0) {
			return false;
		} 
		else if (stall.getStock() == 1) {
			stallDAOImpl.reduceStock(id);
			stallDAOImpl.addSoldOut(id);
			return true;
		}
		else {
			stallDAOImpl.reduceStock(id);
			return true;
		}
	}

	@Override
	public void resetStock() {
		stallDAOImpl.resetStock();
	}

	@Override
	public List<Integer> getSoldOut(Integer id) {
		return stallDAOImpl.getSoldOut(id);
	}

	@Override
	public void addSoldOut(Integer id) {
		stallDAOImpl.addSoldOut(id);
	}
	
	public boolean emptyStaff(StallRequest stallRequest) {
		LOGGER.info("Circuit breaker fallback method triggered!");
		LOGGER.info("Returning empty staff ...");
		return false;
	}
	
}

package com.project.bli.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.bli.entity.Staff;
import com.project.bli.repository.StaffRepository;
import com.project.bli.service.StaffService;

@Service
public class StaffServiceImpl implements StaffService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StaffServiceImpl.class);
	
	@Autowired
	private StaffRepository staffRepository;

	@Override
	public Staff addStaff(Staff staff) {
		LOGGER.info("Add staff service triggered!");
		Staff row = staffRepository.save(staff);
		if (row.getNip().equals("")) {
			LOGGER.error("Failed to add staff to table ...");
			return row;
		}
		LOGGER.info("Add staff potentially successful ...");
		return row;
	}

}

package com.project.bli.service;

import java.util.List;

import com.project.bli.entity.Stall;
import com.project.bli.entity.StallRequest;

public interface StallService {

	List<Stall> getStalls();
	List<Stall> getStallByLocation(String location);
	boolean addStall(StallRequest stallRequest);
	void updateStall(Stall stall);
	void deleteStall(Integer id);
	List<Integer> getAllStallsId();
	Stall getOneStallById(Integer id);
	List<Integer> getStallIdByVendorId(Integer id);
	
	List<Integer> checkStock(String location);
	boolean reduceStock(Integer id);
	void resetStock();
	
	List<Integer> getSoldOut(Integer id);
	void addSoldOut(Integer id);
	
}
